var ParticleFog = cc.Sprite.extend({
	ctor: function() {
		this._super( res.FogParticleSprite );
		
		this.setPosition( this.randomPoint() );
		this.retarget();
		this.setBlendFunc( cc.SRC_ALPHA, cc.ONE )
		this.opacity = 64;
		this.setLocalZOrder( SharedData.isoMap.UnitZOrder( SharedData.isoMap.WorldToIsoMapPosition( cc.p( this.x + 64, this.y - 64 ) ) ) );
	},
	
	randomPoint: function() {
		var row = cc.random0To1() * 576;
		var col = cc.random0To1() * 576;
		var point = cc.p( row + col, (row - col) / 2 );
		
		return point;
	},

	retarget: function() {
		var vector = cc.pForAngle( cc.random0To1() * cc.PI2 );
		cc.pMultIn(vector, 10);
		
		this.runAction( cc.sequence(
				cc.moveBy(cc.random0To1() + 1.5, vector),
				cc.callFunc(this.retarget, this)
				) );
	},
});