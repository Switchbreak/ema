var EditorPaneObjects = [
    [
     	{ sprite: "#UpLeft-1.png", placeObject: Ema },
        { sprite: res.MageSprite, placeObject: Mage, rect: cc.rect(0, 0, 58, 123) },
        { sprite: res.TurtleSprite, placeObject: Turtle, rect: cc.rect(0, 0, 79, 52) },
        { sprite: res.BearSprite, placeObject: Bear, rect: cc.rect( 0, 0, 131, 129 ) },
        { sprite: res.SeagullSprite, placeObject: Seagull, rect: cc.rect( 0, 0, 73, 52 ) },
        { sprite: res.WolfSprite, placeObject: Wolf, rect: cc.rect( 0, 0, 97, 107 ) }
    ],
    [
    ],
    [],
    [
        { sprite: res.GroundTileImage, placeObject: "GroundTile" }
    ]
];

var EditorTabs = {
    Units: 0,
    Enemies: 1,
    Terrain: 2,
    Tiles: 3
};