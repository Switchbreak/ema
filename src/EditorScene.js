var EditorUILayer = null;
var EditorPane = null;

var EditorState = {
    Selecting: 0,
    Placing: 1,
    Painting: 2,
    Erasing: 3
};

var EditorLayer = cc.LayerGradient.extend({
    isoMap: null,
    cursor: null,
    rowsBar: null,
    columnsBar: null,
    addButton: null,
    state: EditorState.Selecting,
    placing: null,
    selectedTab: 0,
    tabButtons: [],
    touchHeld: false,
    defaultDrawState: EditorState.Placing,
    selectButton: null,
    placeButton: null,
    paintButton: null,
    eraseButton: null,
    editorPaneMenu: null,
    unitSelectBox: null,
    
    init: function () {
        this._super( new cc.Color( 0, 0, 128, 255 ), new cc.Color( 0, 0, 255, 255 ), cc.p( 0, 1 ) );
        
        SharedData.gameLayer = this;
        
        this.width -= 100;
        this.height -= 25;
        this.x = 100;
                
        this.isoMap = IsoMap.create( 1, 1 );
        this.isoMap.setPosition( this.width / 2, this.height / 2 );
        this.addChild( this.isoMap );
        
        this.initUI();
        this.initInput();
        this.scheduleUpdate();
    },
    
    update: function( dt ) {
        this.scrollInput( dt );
    },
    
    initUI: function( frameSize ) {
    	cc.spriteFrameCache.addSpriteFrames(res.PlayerSprite);
    	
    	this.cursor = new cc.Sprite( res.CursorSprite ); //cc.Sprite.create( res.CursorSprite );
    	this.cursor.setOpacity( 128 );
    	this.cursor.setVisible( true );
    	this.isoMap.addChild( this.cursor, 2 );
        
        this.columnsBar = Slider.create( Slider.ORIENTATION_HORIZONTAL, this.width - 70 );
        this.columnsBar.setPosition( this.x + 20, 10 );
        EditorUILayer.addChild( this.columnsBar );
        this.columnsBar.minValue = 1;
        this.columnsBar.maxValue = 32;
        this.columnsBar.setCallback( this.scrollChange, this );
        
        this.rowsBar = Slider.create( Slider.ORIENTATION_VERTICAL, this.height - 70 );
        this.rowsBar.setPosition( this.x + this.width - 40, 50 );
        EditorUILayer.addChild( this.rowsBar );
        this.rowsBar.minValue = 1;
        this.rowsBar.maxValue = 32;
        this.rowsBar.setCallback( this.scrollChange, this );

        var menuBackground = new cc.DrawNode();
        var backgroundColor = new cc.Color( 128, 128, 128, 255 );
        menuBackground.drawRect( cc.p( 0, EditorUILayer.height ), cc.p( EditorUILayer.width, this.height ), backgroundColor, 1, backgroundColor );
        EditorUILayer.addChild( menuBackground );
        
        var menu = new cc.Menu();
        menu.setAnchorPoint( 0, 1 );
        menu.setPosition( 110, EditorUILayer.height );
        EditorUILayer.addChild( menu );

        var normalColor = new cc.Color( 128, 128, 128, 255 );
        var selectedColor = new cc.Color( 192, 192, 192, 255 );
        var disabledColor = new cc.Color( 160, 160, 160, 160 );

        var saveButton = this.createMenuButton( res.SaveSprite, normalColor, selectedColor, disabledColor, this.SaveMapFile, this );
        menu.addChild( saveButton );

        var loadButton = this.createMenuButton( res.LoadSprite, normalColor, selectedColor, disabledColor, this.UploadMapFile, this );
        loadButton.x = saveButton.width + 10;
        menu.addChild( loadButton );

        this.selectButton = this.createMenuButton( res.SelectSprite, normalColor, selectedColor, disabledColor, this.SelectMode, this );
        this.selectButton.x = loadButton.x + loadButton.width + 10;
        menu.addChild( this.selectButton );

        this.placeButton = this.createMenuButton( res.PlaceSprite, normalColor, selectedColor, disabledColor, this.PlaceMode, this );
        this.placeButton.x = this.selectButton.x + this.selectButton.width + 10;
        menu.addChild( this.placeButton );

        this.paintButton = this.createMenuButton( res.PaintSprite, normalColor, selectedColor, disabledColor, this.PaintMode, this );
        this.paintButton.x = this.placeButton.x + this.placeButton.width + 10;
        menu.addChild( this.paintButton );

        this.eraseButton = this.createMenuButton( res.EraseSprite, normalColor, selectedColor, disabledColor, this.EraseMode, this );
        this.eraseButton.x = this.paintButton.x + this.paintButton.width + 10;
        menu.addChild( this.eraseButton );

        this.InitEditorPane();
    },    

    createMenuButton: function( sprite, normalColor, selectedColor, disabledColor, callback, target ) {
    	var normalSprite = new cc.Sprite( sprite );
    	normalSprite.color = normalColor;

    	var selectedSprite = new cc.Sprite( sprite );
    	selectedSprite.color = selectedColor;

    	var disabledSprite = new cc.Sprite( sprite );
    	disabledSprite.color = disabledColor;

    	var menuButton = new cc.MenuItemSprite( normalSprite, selectedSprite, disabledSprite, callback, target );
        menuButton.setAnchorPoint( 0, 1 );
        
        return menuButton;
    },
    
    initInput: function () {
    	var touchAction = cc.sequence( [cc.delayTime(0.1), cc.callFunc( function() { this.touchHeld = true; }, SharedData.gameLayer )] );
    	
    	cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed: function(keyCode, event) { SharedData.keyboard[keyCode] = true; },
            onKeyReleased: function(keyCode, event) {
            	SharedData.keyboard[keyCode] = false;

            	if( keyCode == cc.KEY.q )
            		SharedData.gameLayer.ExitEditor();
            	if( keyCode == cc.KEY.f )
            		SharedData.gameLayer.SaveMapFile();

            	SharedData.gameLayer.resizeInput( keyCode );
            }
        }, this);
		
    	cc.eventManager.addListener({
    		event: cc.EventListener.TOUCH_ONE_BY_ONE,
    		onTouchBegan: function( touch, event ) {
        		if( touch.getLocationX() > EditorPane.width ) {
        			SharedData.gameLayer.touchHeld = false;
            		SharedData.gameLayer.runAction( touchAction );
                    return true;
                }
                else if( SharedData.gameLayer.addButton.visible && touch.getLocationY() < 25 ) {
                	SharedData.gameLayer.AddObject();
                }
        		return false;
    		},
    		onTouchMoved: function( touch, event ) {
    			if( SharedData.gameLayer.state == EditorState.Painting || SharedData.gameLayer.state == EditorState.Erasing )
    				SharedData.gameLayer.touchInput( touch, true );
    			else {
    				var delta = cc.pSub( touch.getLocation(), touch.getPreviousLocation() );

    				SharedData.isoMap.x += delta.x;
    				SharedData.isoMap.y += delta.y;
    			}
    		},
    		onTouchEnded: function( touch, event ) {
    			if( !SharedData.gameLayer.touchHeld )
    				SharedData.gameLayer.touchInput( touch, false );
            }
    	}, this);
    },

    scrollInput: function( dt ) {
    	if( SharedData.keyboard[cc.KEY.up] || SharedData.keyboard[cc.KEY.i] )
    		this.isoMap.y -= ScrollSpeed * dt;
    	if( SharedData.keyboard[cc.KEY.down] || SharedData.keyboard[cc.KEY.k] )
    		this.isoMap.y += ScrollSpeed * dt;
    	if( SharedData.keyboard[cc.KEY.left] || SharedData.keyboard[cc.KEY.j] )
    		this.isoMap.x += ScrollSpeed * dt;
    	if( SharedData.keyboard[cc.KEY.right] || SharedData.keyboard[cc.KEY.l] )
    		this.isoMap.x -= ScrollSpeed * dt;
    },

    resizeInput: function( e ) {
    	if( e == cc.KEY.d )
    		this.columnsBar.setValue( this.columnsBar.value + 1 );
    	if( e == cc.KEY.a )
    		this.columnsBar.setValue( this.columnsBar.value - 1 );
    	if( e == cc.KEY.w )
    		this.rowsBar.setValue( this.rowsBar.value + 1 );
    	if( e == cc.KEY.s )
    		this.rowsBar.setValue( this.rowsBar.value - 1 );
    },

    touchInput: function( touch, paint ) {
    	var mapPosition = this.isoMap.WorldToIsoMapPosition( this.isoMap.convertTouchToNodeSpace( touch ) );

    	if( this.isoMap.RangeCheck( mapPosition.row, mapPosition.column ) ) {
    		this.cursor.setPosition( this.isoMap.IsoMapToWorldPosition( mapPosition ) );

    		if( (!paint && this.state == EditorState.Placing) || this.state == EditorState.Painting ) {
    			this.PlaceObjectOnMap( mapPosition );
    		}
    		else if( this.state == EditorState.Erasing ) {
    			this.EraseObjectOnMap( mapPosition );
    		}
    	}
    },

    PlaceObjectOnMap: function( mapPosition ) {
    	var mapTile = this.isoMap.mapTiles[mapPosition.row][mapPosition.column];

    	if( typeof( this.placing.placeObject ) != "string" ) {
    		if( mapTile.walkable && mapTile.unit == null ) {
    			this.isoMap.AddUnit( this.placing.placeObject.create(), mapPosition );
    			return true;
    		}
    	}
    	else if( this.placing.placeObject === "TerrainObject" ) {
    		this.isoMap.SetTerrainObject( mapPosition, this.placing.sprite, this.placing.anchor, this.placing.rect );
    		return true;
    	}
    	else if( this.placing.placeObject === "GroundTile" ) {
    		this.isoMap.SetGroundTile( mapPosition, this.placing.sprite, this.placing.anchor, this.placing.rect );
    		return true;
    	}

    	return false;
    },

    EraseObjectOnMap: function( mapPosition ) {
    	var mapTile = this.isoMap.mapTiles[mapPosition.row][mapPosition.column];

    	if( mapTile.unit )
    		this.isoMap.RemoveUnit( mapTile.unit );
    	this.isoMap.SetTerrainObject( mapPosition, null );
    	this.isoMap.SetGroundTile( mapPosition, res.GroundTileImage );
    },

    scrollChange: function() {
    	this.isoMap.ResizeMap( this.rowsBar.value, this.columnsBar.value );
    },

    ExitEditor: function () {
    	cc.director.runScene( new MenuScene() );
    },

    SaveMapFile: function () {
    	var output = this.isoMap.SaveToXML();

    	var downloadElement = document.createElement( "a" );
    	downloadElement.setAttribute( "href", "data:text/xml;charset=utf-8," + encodeURIComponent( output ) );
    	downloadElement.setAttribute( "download", "level.xml" );
    	document.documentElement.appendChild( downloadElement );
    	downloadElement.click();
    },

    LoadMapFile: function( mapXML ) {
    	var frameSize = cc.director.getWinSize();
    	frameSize.width -= 100;

    	this.removeChild( this.isoMap );

    	this.isoMap = IsoMap.createWithXML( mapXML );
    	this.isoMap.setPosition( frameSize.width / 2, frameSize.height / 2 );
    	this.addChild( this.isoMap );

    	this.rowsBar.initValue( this.isoMap.rows );
    	this.columnsBar.initValue( this.isoMap.columns );
    },
    
    SelectMode: function() {
        this.state = EditorState.Selecting;
        this.placing = null;
        
        this.selectButton.enabled = false;
        this.placeButton.enabled = true;
        this.paintButton.enabled = true;
        this.eraseButton.enabled = true;
        
        this.unitSelectBox.visible = false;
    },
    
    PlaceMode: function() {
        this.defaultDrawState = EditorState.Placing;
        if( this.placing ) {
            this.state = EditorState.Placing;
        }
        
        this.selectButton.enabled = true;
        this.placeButton.enabled = false;
        this.paintButton.enabled = true;
        this.eraseButton.enabled = true;
    },
    
    PaintMode: function() {
        this.defaultDrawState = EditorState.Painting;
        if( this.placing ) {
            this.state = EditorState.Painting;
        }
        
        this.selectButton.enabled = true;
        this.placeButton.enabled = true;
        this.paintButton.enabled = false;
        this.eraseButton.enabled = true;
    },
    
    EraseMode: function() {
        this.state = EditorState.Erasing;
        this.placing = null;
        
        this.selectButton.enabled = true;
        this.placeButton.enabled = true;
        this.paintButton.enabled = true;
        this.eraseButton.enabled = false;
    },
    
    UploadMapFile: function() {
        var element = document.createElement( "div" );
        var template = cc.loader.getRes( res.UploadBox );
        element.innerHTML = template;
        document.body.appendChild( element );
        
        var dialogBox = document.getElementById( "UploadObject" );
        var closeButton = document.getElementById( "UploadObjectClose" );
        var uploadFile = document.getElementById( "UploadObjectFile" );
        var previewArea = document.getElementById( "UploadObjectPreview" );
        var confirmArea = document.getElementById( "UploadObjectConfirm" );
        var addButton = document.getElementById( "UploadObjectConfirmAdd" );
        
        var hideFunction = function() {
            document.body.removeChild( element );
            return false;
        };
        
        var centerDialog = function() {
            dialogBox.style.top = ((window.innerHeight / 2) - (dialogBox.clientHeight / 2)) + "px";
            dialogBox.style.left = ((window.innerWidth / 2) - (dialogBox.clientWidth / 2)) + "px";
        };
        
        dialogBox.style.display = "block";
        centerDialog();
        closeButton.onclick = hideFunction;
        
        uploadFile.addEventListener( "change", function(e) {
            var file = uploadFile.files[0];
            if( file.type = "text.xml" ) {
                var reader = new FileReader();
                    
                reader.onload = function(e) {
                    SharedData.gameLayer.LoadMapFile( reader.result );
                    hideFunction();
                };
                    
                reader.readAsText(file);
            }
            else {
                previewArea.innerHTML = "File not supported.";
            }
        });
    },
    
    ChangeTab: function( tabIndex ) {
        this.selectedTab = tabIndex;
        
        this.InitEditorTab( tabIndex );
    },
    
    AddObject: function() {
        var element = document.createElement( "div" );
        var template = cc.loader.getRes( res.UploadBox );
        element.innerHTML = template;
        document.body.appendChild( element );
        
        var dialogBox = document.getElementById( "UploadObject" );
        var closeButton = document.getElementById( "UploadObjectClose" );
        var uploadFile = document.getElementById( "UploadObjectFile" );
        var previewArea = document.getElementById( "UploadObjectPreview" );
        var confirmArea = document.getElementById( "UploadObjectConfirm" );
        var addButton = document.getElementById( "UploadObjectConfirmAdd" );
        var anchorImage = document.getElementById( "UploadObjectAnchor" );
        var anchorText = document.getElementById( "UploadObjectConfirmAnchor" );
        
        var hideFunction = function() {
            document.body.removeChild( element );
            return false;
        };
        
        var centerDialog = function() {
            dialogBox.style.top = ((window.innerHeight / 2) - (dialogBox.clientHeight / 2)) + "px";
            dialogBox.style.left = ((window.innerWidth / 2) - (dialogBox.clientWidth / 2)) + "px";
        };
        
        dialogBox.style.display = "block";
        centerDialog();
        closeButton.onclick = hideFunction;
        
        uploadFile.addEventListener( "change", function(e) {
            var file = uploadFile.files[0];
            if( file.type.match( /image.*/ ) ) {
                var reader = new FileReader();
                
                reader.onload = function(e) {
                    var img = new Image();
                    img.src = reader.result;
                    img.style.position = "relative";
                    img.style.zIndex = 2;
                    
                    previewArea.innerHTML = "";
                    previewArea.appendChild( img );
                    
                    confirmArea.style.display = "block";
                    
                    if( SharedData.gameLayer.selectedTab == EditorTabs.Terrain ) {
                        var anchorX = 0.5;
                        var anchorY = 0.2;
                        
                        var setAnchorPoint = function( anchorX, anchorY ) {
                            anchorText.style.display = "block";
                            anchorText.innerHTML = "(" + anchorX + ", " + anchorY + ")";
                        
                            var dialogRect = dialogBox.getBoundingClientRect();
                            var rect = img.getBoundingClientRect();
                        
                            var offsetX = (rect.right - rect.left) * anchorX;
                            var offsetY = (rect.bottom - rect.top) * anchorY;
                            
                            anchorImage.style.display = "block";
                            anchorImage.style.top = (rect.bottom - dialogRect.top - 32 - offsetY) + "px";
                            anchorImage.style.left = (rect.left - dialogRect.left - 64 + offsetX) + "px";
                        };
                    
                        img.onload = function() {
                            centerDialog();
                            setAnchorPoint( anchorX, anchorY );
                        };
                    
                        img.onmousedown = function(e) {
                            var rect = img.getBoundingClientRect();
                        
                            anchorX = (e.clientX - rect.left) / (rect.right - rect.left);
                            anchorY = (rect.bottom - e.clientY) / (rect.bottom - rect.top);
                        
                            setAnchorPoint( anchorX, anchorY );
                        };
                        
                        addButton.onclick = function() {
                            SharedData.gameLayer.AddObjectImage( img, SharedData.gameLayer.selectedTab, cc.p( anchorX, anchorY ) );
                            return hideFunction();
                        };
                    }
                    else {
                        img.onload = centerDialog;
                        
                        addButton.onclick = function() {
                            SharedData.gameLayer.AddObjectImage( img, SharedData.gameLayer.selectedTab );
                            return hideFunction();
                        };
                    }
                };
                    
                reader.readAsDataURL(file);
            }
            else {
                previewArea.innerHTML = "File not supported.";
            }
        });
    },
    
    AddObjectImage: function( img, tab, anchorPoint ) {
        var namePrefix;
        switch (tab) {
            case EditorTabs.Terrain:
                namePrefix = "TerrainObject";
                break;
            case EditorTabs.Tiles:
                namePrefix = "GroundTile";
                break;
            default:
                return;
        }
        
        var imageName = namePrefix + EditorPaneObjects[tab].length;
        cc.textureCache.cacheImage( imageName, img );
        EditorPaneObjects[tab].push( { sprite: imageName, placeObject: namePrefix, image: img, anchor: anchorPoint } );
        this.InitEditorTab( this.selectedTab );
    },
    
    EditObject: function( editorPaneObject, index ) {
        var element = document.createElement( "div" );
        var template = cc.loader.getRes( res.UploadBox );
        element.innerHTML = template;
        document.body.appendChild( element );
        
        var dialogBox = document.getElementById( "UploadObject" );
        var closeButton = document.getElementById( "UploadObjectClose" );
        var uploadFile = document.getElementById( "UploadObjectFile" );
        var previewArea = document.getElementById( "UploadObjectPreview" );
        var confirmArea = document.getElementById( "UploadObjectConfirm" );
        var editButton = document.getElementById( "UploadObjectConfirmAdd" );
        var deleteButton = document.getElementById( "UploadObjectConfirmDelete" );
        var anchorImage = document.getElementById( "UploadObjectAnchor" );
        var anchorText = document.getElementById( "UploadObjectConfirmAnchor" );
        
        var hideFunction = function() {
            document.body.removeChild( element );
            return false;
        };
        
        var centerDialog = function() {
            dialogBox.style.top = ((window.innerHeight / 2) - (dialogBox.clientHeight / 2)) + "px";
            dialogBox.style.left = ((window.innerWidth / 2) - (dialogBox.clientWidth / 2)) + "px";
        };
        
        var loadImg = function( img, anchor ) {
	        img.style.position = "relative";
	        img.style.zIndex = 2;
	        
	        previewArea.innerHTML = "";
	        previewArea.appendChild( img );
	        
	        confirmArea.style.display = "block";
	        
	        if( SharedData.gameLayer.selectedTab == EditorTabs.Terrain ) {
	            var setAnchorPoint = function( anchor ) {
	                anchorText.style.display = "block";
	                anchorText.innerHTML = "(" + anchor.x + ", " + anchor.y + ")";
	            
	                var dialogRect = dialogBox.getBoundingClientRect();
	                var rect = img.getBoundingClientRect();
	            
	                var offsetX = (rect.right - rect.left) * anchor.x;
	                var offsetY = (rect.bottom - rect.top) * anchor.y;
	                
	                anchorImage.style.display = "block";
	                anchorImage.style.top = (rect.bottom - dialogRect.top - 32 - offsetY) + "px";
	                anchorImage.style.left = (rect.left - dialogRect.left - 64 + offsetX) + "px";
	            };
	        
	            img.onload = function() {
	                centerDialog();
	                setAnchorPoint( anchor );
	            };
	        
	            img.onmousedown = function(e) {
	                var rect = img.getBoundingClientRect();
	            
	                anchor.x = (e.clientX - rect.left) / (rect.right - rect.left);
	                anchor.y = (rect.bottom - e.clientY) / (rect.bottom - rect.top);
	            
	                setAnchorPoint( anchor );
	            };
	            
	            editButton.onclick = function() {
	            	editorPaneObject.image = img;
	            	editorPaneObject.anchor = anchor;
	                SharedData.gameLayer.UpdateObjectOnMap( editorPaneObject );
	            	
	                return hideFunction();
	            };

	            deleteButton.onclick = function() {
	            	SharedData.gameLayer.DeleteObjectOnMap( editorPaneObject, index );

	            	return hideFunction();
	            };
	        }
	        else {
	            img.onload = centerDialog;
	            
	            editButton.onclick = function() {
	            	editorPaneObject.image = img;
	                SharedData.gameLayer.UpdateTileOnMap( editorPaneObject );
	            	
	                return hideFunction();
	            };

	            deleteButton.onclick = function() {
	            	SharedData.gameLayer.DeleteTileOnMap( editorPaneObject, index );

	            	return hideFunction();
	            };
	        }
        }
        
        loadImg( editorPaneObject.image, editorPaneObject.anchor );
        editorPaneObject.image.onload();
        
        editButton.innerHTML = "Update";
        deleteButton.style.display = "inline";
        dialogBox.style.display = "block";
        
        closeButton.onclick = hideFunction;
        
        centerDialog();
        
        uploadFile.addEventListener( "change", function(e) {
            var file = uploadFile.files[0];
            if( file.type.match( /image.*/ ) ) {
                var reader = new FileReader();
                
                reader.onload = function(e) {
                    var img = new Image();
                    img.src = reader.result;
                    
                    loadImg( img, cc.p( 0.5, 0.2 ) );
                };
                    
                reader.readAsDataURL(file);
            }
            else {
                previewArea.innerHTML = "File not supported.";
            }
        });
    },
    
    UpdateObjectOnMap: function( editorPaneObject ) {
    	cc.textureCache.cacheImage( editorPaneObject.sprite, editorPaneObject.image );
    	
    	for( var row = 0; row < this.isoMap.rows; row++ ) {
    		for( var column = 0; column < this.isoMap.columns; column++ ) {
    			var mapTile = this.isoMap.mapTiles[row][column]
    			if( mapTile.terrainName == editorPaneObject.sprite ) {
    				mapTile.terrainObject.setTexture( editorPaneObject.sprite );
    				mapTile.terrainObject.setAnchorPoint( editorPaneObject.anchor );
    			}
    		}
    	}
    	
    	this.InitEditorTab( this.selectedTab );
    },
    
    DeleteObjectOnMap: function( editorPaneObject, index ) {
    	for( var row = 0; row < this.isoMap.rows; row++ ) {
    		for( var column = 0; column < this.isoMap.columns; column++ ) {
    			var mapTile = this.isoMap.mapTiles[row][column]
    			if( mapTile.terrainName == editorPaneObject.sprite ) {
    				this.isoMap.SetTerrainObject( { row: row, column: column }, null );
    			}
    		}
    	}
    	
        EditorPaneObjects[this.selectedTab].splice( index, 1 );
    	this.InitEditorTab( this.selectedTab );
    	
    	cc.textureCache.removeTextureForKey( editorPaneObject.sprite );
    },

    UpdateTileOnMap: function( editorPaneObject ) {
    	cc.textureCache.cacheImage( editorPaneObject.sprite, editorPaneObject.image );

    	for( var row = 0; row < this.isoMap.rows; row++ ) {
    		for( var column = 0; column < this.isoMap.columns; column++ ) {
    			var mapTile = this.isoMap.mapTiles[row][column]
    			if( mapTile.tileName == editorPaneObject.sprite )
    				mapTile.groundTile.setTexture( editorPaneObject.sprite );
    		}
    	}

    	this.InitEditorTab( this.selectedTab );
    },

    DeleteTileOnMap: function( editorPaneObject, index ) {
    	for( var row = 0; row < this.isoMap.rows; row++ ) {
    		for( var column = 0; column < this.isoMap.columns; column++ ) {
    			var mapTile = this.isoMap.mapTiles[row][column]
    			if( mapTile.tileName == editorPaneObject.sprite )
    				this.isoMap.SetGroundTile( { row: row, column: column }, res.GroundTileImage );
    		}
    	}

    	EditorPaneObjects[this.selectedTab].splice( index, 1 );
    	this.InitEditorTab( this.selectedTab );

    	cc.textureCache.removeTextureForKey( editorPaneObject.sprite );
    },
    
    InitEditorPane: function() {
        this.selectedTab = 0;
        
        var tabColor = new cc.Color( 128, 128, 128, 255 );
        var selectedColor = new cc.Color( 192, 192, 192, 255 );
        var disabledColor = new cc.Color( 160, 160, 160, 255 );
        var tabs = [res.UnitSprite, res.EnemySprite, res.TerrainSprite, res.TileSprite];
        
        var tabMenu = new cc.Menu();
        tabMenu.setPosition( 0, EditorUILayer.height );
        
        var tabCallback = function( tabIndex ) { return function() { this.ChangeTab( tabIndex ); }; };
                
        for( var i = 0; i < 4; i++ ) {
        	var tab = this.createMenuButton( tabs[i], tabColor, selectedColor, disabledColor, tabCallback( i ), this );
            tab.setPosition( i * 25, 0 );
            
            this.tabButtons[i] = tab;
            tabMenu.addChild( tab );
        }
        
        EditorUILayer.addChild( tabMenu );
        
        var color = new cc.Color( 128, 128, 128, 255 );
        this.addButton = new cc.DrawNode();
        this.addButton.drawRect( cc.p( 0, 0 ), cc.p( 100, 40 ), color, 1, color );
        
        var buttonLabel = new cc.LabelTTF( "+", "Ariel", 32 );
        buttonLabel.setFontFillColor( new cc.Color( 0, 0, 0, 255 ) );
        buttonLabel.setAnchorPoint( 0.5, 0 );
        buttonLabel.setPosition( 50, 1 );
        this.addButton.addChild( buttonLabel );
        
        EditorUILayer.addChild( this.addButton );
        
        this.unitSelectBox = new cc.DrawNode();
        this.unitSelectBox.setAnchorPoint( 0, 1 );
        
        this.InitEditorTab( 0 );
    },
    
    InitEditorTab: function( tabIndex ) {
        EditorPane.container.removeAllChildren();
        
        EditorPane.addChild( this.unitSelectBox );
        this.SelectMode();
        
        var buttonPosition = -10;
        var tabObjects = EditorPaneObjects[tabIndex];
        
        this.editorPaneMenu = new cc.Menu();
        this.editorPaneMenu.setAnchorPoint( 0, 1 );
        
        var placeCallback = function( tabObject, index ) {
        	return function() {
        		if( this.placing == tabObject )
        			this.EditObject(tabObject, index);
        		else
        			this.PlaceObject(tabObject, index);
        	};
        };
        
        for( var i = 0; i < tabObjects.length; i++ ) {
            var buttonSprites = [];
            for( var j = 0; j < 2; j++ )
                buttonSprites[j] = new cc.Sprite( tabObjects[i].sprite, tabObjects[i].rect );
            
            var button = new cc.MenuItemSprite( buttonSprites[0], buttonSprites[1], placeCallback( tabObjects[i], i ), this );
            button.setAnchorPoint( 0.5, 1 );
            button.setPosition( 45, buttonPosition );
            if( button.width > 90 ) {
                var scale = 90 / button.width;
                button.scale = scale;
            }
            this.editorPaneMenu.addChild( button );
        
            buttonPosition -= button.height * button.scale + 10;
        }
        
        EditorPane.setContentSize( cc.size( 100, -buttonPosition ) );
        EditorPane.setContentOffset( EditorPane.minOffset, false )
        this.editorPaneMenu.setPosition( 0, -buttonPosition );
        EditorPane.addChild( this.editorPaneMenu );

        if( tabIndex > 1 )
            this.addButton.visible = true;
        else
            this.addButton.visible = false;
            
        for( var i = 0; i < this.tabButtons.length; i++ ) {
            this.tabButtons[i].enabled = (i != tabIndex);
        }
    },
    
    PlaceObject: function( placeObject, index ) {
        this.placing = placeObject;
        if( this.defaultDrawState == EditorState.Placing )
            this.PlaceMode();
        if( this.defaultDrawState == EditorState.Painting )
            this.PaintMode();
        
        var menuItem = this.editorPaneMenu.children[index];
        var color = new cc.Color( 50, 50, 50, 128 );
        this.unitSelectBox.visible = true;
        this.unitSelectBox.clear();
        this.unitSelectBox.drawRect( cc.p( 0, 0 ), cc.p( 100, -menuItem.height * menuItem.scale ), color, 1, color );
        this.unitSelectBox.y = this.editorPaneMenu.y + menuItem.y;
    }
});


var EditorScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        
        var frameSize = cc.director.getWinSize();
        var paneBackground = new cc.LayerGradient( new cc.Color( 192, 192, 192, 255 ), new cc.Color( 128, 128, 128, 255 ), cc.p( 1, 0 ) );
        paneBackground.setContentSize( 100, frameSize.height - 25 );
        EditorPane = new cc.ScrollView( cc.size( 100, frameSize.height - 65 ), new cc.Layer() );
        EditorPane.y = 40;
        EditorPane.direction = cc.SCROLLVIEW_DIRECTION_VERTICAL;
        
        EditorUILayer = new cc.Layer();
        var layer = new EditorLayer();
        
        this.addChild(layer);
        this.addChild(paneBackground);
        this.addChild(EditorPane);
        this.addChild(EditorUILayer);
        
        EditorUILayer.init();
        layer.init();        
    }
});
