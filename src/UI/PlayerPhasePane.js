var PlayerPhasePane = cc.LayerGradient.extend({
    endTurnButton: null,
    nextButton: null,
    prevButton: null,
    playerName: null,
    playerHP: null,
    playerPhase: null,
        
    ctor: function( endTurn, next, prev ) {
        var frameSize = cc.director.getWinSize();
        
        this._super( new cc.Color( 128, 128, 128, 255 ), new cc.Color( 64, 64, 64, 255 ), cc.p( 0, -1 ) );
        this.setContentSize( frameSize.width, 75 );
        this.setAnchorPoint( 0, 0 );
        
        this.endTurnButton = new cc.MenuItemFont( "End Turn", endTurn, SharedData.gameLayer );
        this.endTurnButton.setAnchorPoint( 0, 0.5 );
        this.endTurnButton.setPosition( 10, 32 );
        
        this.nextButton = new cc.MenuItemFont( "Next", next, SharedData.gameLayer );
        this.nextButton.setAnchorPoint( 1, 0.5 );
        this.nextButton.setPosition( frameSize.width - 10, 32 );
        
        this.prevButton = new cc.MenuItemFont( "Previous", prev, SharedData.gameLayer );
        this.prevButton.setAnchorPoint( 1, 0.5 );
        this.prevButton.setPosition( this.nextButton.x - this.nextButton.width - 10, 32 );
        
        var menu = new cc.Menu( this.endTurnButton, this.nextButton, this.prevButton );
        menu.setPosition( 0, 0 );
        this.addChild( menu );
        
        this.playerName = new cc.LabelTTF( "Name" );
        this.playerName.setAnchorPoint( 0, 1 );
        this.playerName.setPosition( this.endTurnButton.width + 30, 70 );
        this.addChild( this.playerName );
        
        this.playerHP = new cc.LabelTTF( "HP: " );
        this.playerHP.setAnchorPoint( 0, 1 );
        this.playerHP.setPosition( this.playerName.x, this.playerName.y - this.playerName.height );
        this.addChild( this.playerHP );
        
        this.playerPhase = new cc.LabelTTF( "Remaining Actions: " );
        this.playerPhase.setAnchorPoint( 0, 1 );
        this.playerPhase.setPosition( this.playerHP.x, this.playerHP.y - this.playerHP.height );
        this.addChild( this.playerPhase );
    },
    
    updatePanel: function( unit ) {
        this.playerName.string = unit.name;
        this.playerHP.string = "HP: " + unit.HP;
        this.playerPhase.string = "Remaining Actions: " + (PhaseCount - unit.phase);
    }
});