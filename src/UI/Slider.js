var Slider = cc.Node.extend({
    scroller: null,
    orientation: 0,
    length: 0,
    value: 0,
    minValue: 0,
    maxValue: 100,
    callback: null,
    callbackTarget: null,
    
    init: function( orientation, length ) {
        this._super();
        
        this.orientation = orientation;
        this.length = length;
        
        if( orientation == Slider.ORIENTATION_HORIZONTAL ) {
            var bar = cc.DrawNode.create();
            bar.drawSegment( cc.p( 0, 15 ), cc.p(length, 15), 4, new cc.Color( 50, 50, 50, 255 ) );
            this.addChild( bar );
        
            this.scroller = cc.DrawNode.create();
            this.scroller.drawSegment( cc.p( 0, 0 ), cc.p(0, 30), 6, new cc.Color( 128, 128, 128, 255 ) );
            this.addChild( this.scroller );
        }
        else {
            var bar = cc.DrawNode.create();
            bar.drawSegment( cc.p( 15, 0 ), cc.p(15, length), 4, new cc.Color( 50, 50, 50, 255 ) );
            this.addChild( bar );
        
            this.scroller = cc.DrawNode.create();
            this.scroller.drawSegment( cc.p( 0, 0 ), cc.p(30, 0), 6, new cc.Color( 128, 128, 128, 255 ) );
            this.scroller.y = this.length;
            this.addChild( this.scroller );
        }
        
        this.initInput();
        
        return true;
    },
    
    initInput: function() {
        var target = this;
        
        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            onTouchBegan: function(touch, event) { return target.onTouchBegan.call( target, touch, event ); },
            onTouchMoved: function(touch, event) { target.onTouchMoved.call( target, touch, event ); },
            onTouchEnded: function(touch, event) { target.onTouchEnded.call( target, touch, event ); },
            swallowTouches: true
        }, this);
    },
    
    setCallback: function( callback, target ) {
        this.callback = callback;
        this.callbackTarget = target;
    },
    
    setValue: function( value ) {
        if( this.initValue( value ) && this.callback != null )
            this.callback.call( this.callbackTarget );
    },
    
    initValue: function( value ) {
        if( value < this.minValue )
            value = this.minValue;
        if( value > this.maxValue )
            value = this.maxValue;
        
        if( this.value == value )
            return false;
        
        this.value = value;
        
        var position = this.length * (this.value - this.minValue) / (this.maxValue - this.minValue);
        
        if( this.orientation == Slider.ORIENTATION_HORIZONTAL ) {
            this.scroller.x = position;
        }
        else {
            this.scroller.y = this.length - position;
        }
        
        return true;
    },
    
    getScrollerRect: function() {
        if( this.orientation == Slider.ORIENTATION_HORIZONTAL ) {
            return cc.rect( this.scroller.x - 20, this.scroller.y, 40, 30 );
        }
        else {
            return cc.rect( this.scroller.x, this.scroller.y - 20, 30, 40 );
        }
    },
    
    onTouchBegan: function( touch, event ) {
        var touchPosition = this.convertTouchToNodeSpace( touch );
        
        if( cc.rectContainsPoint( this.getScrollerRect(), touchPosition ) ) {
            this.isDragging = true;
        }
        
        return this.isDragging;
    },
    
    onTouchMoved: function( touch, event ) {
        var touchPosition = this.convertTouchToNodeSpace( touch );
            
        var value;
        if( this.orientation == Slider.ORIENTATION_HORIZONTAL ) {
            value = this.minValue + (touchPosition.x / this.length) * (this.maxValue - this.minValue);
        }
        else {
            value = this.minValue + ((this.length - touchPosition.y) / this.length) * (this.maxValue - this.minValue);
        }
            
        this.setValue( Math.floor( value ) );
    },
    
    onTouchEnded: function( touch, event ) {
        this.isDragging = false;
    }
});

Slider.ORIENTATION_VERTICAL = 0;
Slider.ORIENTATION_HORIZONTAL = 1;

Slider.create = function( orientation, length ) {
    var slider = new Slider();
    if( slider && slider.init( orientation, length ) )
            return slider;
    return null; 
};