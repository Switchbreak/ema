var Floater = cc.LabelTTF.extend({
    init: function( floaterText, color ) {
        this.initWithString( floaterText, "Arial", 32 );
        this.setFontFillColor( color );
        this.enableStroke( new cc.Color( 0, 0, 0, 255 ), 1 );
        this.setAnchorPoint( 0.5, 0.5 );
        this.runAction( cc.MoveBy.create( 0.5, cc.p( 0, 32 ) ) );
        this.runAction( cc.Sequence.create( cc.FadeOut.create( 0.5 ), cc.CallFunc.create( this.RemoveFloater, this ) ) );
        
        return true;
    },
    
    RemoveFloater: function () {
        this.getParent().removeChild( this );
    }
});

Floater.create = function(floaterText, color) {
    var floater = new Floater();
    if( floater && floater.init( floaterText, color ) )
        return floater;
    
    return null;
};