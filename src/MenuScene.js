var MenuLayer = cc.Layer.extend({
    init: function () {
        var frameSize = cc.director.getWinSize();

        var title = cc.LabelTTF.create( "Ema", "Arial", 40 );
        title.setAnchorPoint( 0.5, 0.5 );
        title.setPosition( frameSize.width / 2, 3 * frameSize.height / 4 );
        this.addChild( title );
        
        var newGame = cc.MenuItemFont.create( "New Game", this.NewGame, this );
        
        var levelEditor = cc.MenuItemFont.create( "Level Editor", this.LevelEditor, this );
        levelEditor.setPositionY( -60 );
        
        var menu = cc.Menu.create( newGame, levelEditor );
        this.addChild( menu );
        
        this._super();
    },
    
    NewGame: function () {
        cc.director.runScene( new EmaScene() );
    },
    
    LevelEditor: function () {
        cc.director.runScene( new EditorScene() );
    }
});

var MenuScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        
        var layer = new MenuLayer();
        
        this.addChild(layer);
        
        layer.init();        
    }
});
