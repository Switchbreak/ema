var res = {
	BackgroundImage : "res/Background.png",
    GroundTileImage : "res/GroundTile.png",
    PlayerSpriteImg : "res/Ema-Sprite.png",
    PlayerSprite : "res/Ema-Sprite.plist",
    MageSprite : "res/MageSketchSprite.png",
    BearSprite : "res/Bear.png",
    SeagullSprite : "res/SeaGull.png",
    WolfSprite : "res/Wolf.png",
    TurtleSprite : "res/Turtle.png",
    CursorSprite : "res/SelectTile.png",
    SaveSprite : "res/UI/download.png",
    LoadSprite : "res/UI/upload.png",
    SelectSprite : "res/UI/pipette.png",
    PlaceSprite : "res/UI/pencil.png",
    PaintSprite : "res/UI/brush.png",
    EraseSprite : "res/UI/delete.png",
    UnitSprite : "res/UI/user.png",
    EnemySprite : "res/UI/eye.png",
    TerrainSprite : "res/UI/tree.png",
    TileSprite : "res/UI/th-large.png",
    UploadBox : "res/UI/uploadBox.xml",
    Level1XML : "res/Levels/level.xml",
	Level2XML : "res/Levels/level9.xml",
	ParticleSprite : "res/Particle.png",
	FogParticleSprite : "res/FogParticle.png"
};

var g_resources = [
    // Tiles
    res.BackgroundImage,
    res.GroundTileImage,
    
    // Unit sprites
    res.PlayerSpriteImg,
    res.PlayerSprite,
    res.MageSprite,
    res.BearSprite,
    res.SeagullSprite,
    res.WolfSprite,
    res.TurtleSprite,
    
    // UI images
    res.CursorSprite,
    res.SaveSprite,
    res.LoadSprite,
    res.SelectSprite,
    res.PlaceSprite,
    res.PaintSprite,
    res.EraseSprite,
    res.UnitSprite,
    res.EnemySprite,
    res.TerrainSprite,
    res.TileSprite,
    
    // UI resources
    res.UploadBox,
    
    // Textures
    res.ParticleSprite,
    res.FogParticleSprite,
    
    //levels
    res.Level1XML,
    res.Level2XML
];