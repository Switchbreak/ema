var tileWidth = 128;
var tileHeight = 64;

var IsoMap = cc.Node.extend({
    mapTiles: [],
    rows: 0,
    columns: 0,
    highlightSquares: [],
    units: [],
    
    init: function (rows, columns) {
        this._super();
        
        this.rows = rows;
        this.columns = columns;
        
        var rowPosition = cc.p( 0, 0 );
        for( var row = 0; row < rows; row++ ) {
            this.mapTiles[row] = [];
            var position = cc.p( rowPosition.x, rowPosition.y );
			
            for( var column = 0; column < columns; column++ ) {
                this.mapTiles[row][column] = this.AddTile( position );
				
                position.x += tileWidth / 2;
                position.y += tileHeight / 2;
            }
			
            rowPosition.x += tileWidth / 2;
            rowPosition.y -= tileHeight / 2;
        }

        var dropShadow = new cc.DrawNode();
        
        var vertices = [ cc.p( 0, 0 ),
                         cc.p( this.columns * tileWidth / 2, this.columns * tileHeight / 2 ),
                         cc.p( this.columns * tileWidth / 2 + this.rows * tileWidth / 2, this.columns * tileWidth / 2 - this.rows * tileWidth / 2 ),
                         cc.p( this.rows * tileWidth / 2, -this.rows * tileHeight / 2 ) ];
        dropShadow.drawPoly( vertices, new cc.Color( 0, 0, 0, 128 ), 0, new cc.Color( 0, 0, 0, 128 ) );
        
        dropShadow.x = -tileWidth / 2 + 2;
        dropShadow.y = -4;
        
        this.addChild( dropShadow );
        
        SharedData.isoMap = this;
        
        return true;
    },
    
    initWithFile: function( filename ) {
        var fileContents = cc.loader.getRes( filename );
        return this.initWithXML( fileContents );
    },
    
    initWithXML: function( xml ) {
        var parser = new cc.SAXParser();
        var xmlDoc = parser.parse( xml );
        
        this.ParseMap(xmlDoc);
        
        xmlDoc = null;
        
        return true;
    },

    ParseMap: function( xmlDoc ) {
        var levelElement = xmlDoc.documentElement;
        if( levelElement.tagName != 'IsoLevel' )
            throw "IsoLevel file invalid";
        
        cc.log( "Root found" );
    	
        var imageAttributes = [];
        this.ParseEditorTab(this.GetSingleElement(levelElement, "TerrainImages"), EditorTabs.Terrain, imageAttributes);
        this.ParseEditorTab(this.GetSingleElement(levelElement, "Tiles"), EditorTabs.Tiles, imageAttributes);
        
        cc.log( "Image assets loaded" );
    	
        var mapElement = this.GetSingleElement(levelElement, "Map");
        var rows = parseInt( mapElement.getAttribute( "rows" ) );
        var columns = parseInt( mapElement.getAttribute( "columns" ) );
        
        cc.log( "Initializing map. Rows: " + rows + " - Columns: " + columns );
        if( !this.init(rows, columns) )
            return false;
        cc.log( "Map initialized" );
    	
        this.ParseMapTiles(mapElement, imageAttributes);
        cc.log( "Tiles loaded" );
        this.ParseUnits(this.GetSingleElement(levelElement, "Units"));
        cc.log( "Units loaded" );
    },
    
    GetSingleElement: function( parentElement, childName ) {
        var elements = parentElement.getElementsByTagName(childName);
        if( elements.length != 1 )
            throw "IsoLevel file invalid";
    	
        return elements[0];
    },
    
    ParseEditorTab: function ( tabElement, editorTab, imageAttributes ) {
        var len = tabElement.childNodes.length;
        for( var i = 0; i < len; i++ ) {
            this.ParseEditorImage(tabElement.childNodes[i], editorTab, imageAttributes);
        }
    },
    
    ParseEditorImage: function( imageElement, editorTab, imageAttributes ) {
        var img = new Image();
        img.src = imageElement.childNodes[0].nodeValue;
    
        var name = imageElement.getAttribute( "name" );
        var anchorX = imageElement.getAttribute( "anchor-x" );
        var anchorY = imageElement.getAttribute( "anchor-y" );
        var rectX = imageElement.getAttribute( "rect-x" );
        var rectY = imageElement.getAttribute( "rect-y" );
        var rectWidth = imageElement.getAttribute( "rect-width" );
        var rectHeight = imageElement.getAttribute( "rect-height" );
        
        var anchorPoint = null;
        if( anchorX != null && anchorY != null )
            anchorPoint = cc.p( anchorX, anchorY );
        
        var rect = null;
        if( rectX != null && rectY != null && rectWidth != null && rectHeight != null )
            rect = cc.rect( rectX, rectY, rectWidth, rectHeight );
        
        imageAttributes[name] = { anchor: anchorPoint };
        
        if( SharedData.gameLayer instanceof EditorLayer ) {
            SharedData.gameLayer.AddObjectImage(img, editorTab, anchorPoint);
        }
        else {
            cc.textureCache.cacheImage( name, img );
        }
    },
    
    ParseMapTiles: function( mapElement, imageAttributes ) {
        if( mapElement.childNodes.length != this.rows )
            throw "IsoLevel file invalid";
    	
        var mapPosition = {row: 0, column: 0};
        for( mapPosition.row = 0; mapPosition.row < this.rows; mapPosition.row++ ) {
            var rowElement = mapElement.childNodes[mapPosition.row];
            if( rowElement.childNodes.length != this.columns )
                throw "IsoLevel file invalid";
    		
            for( mapPosition.column = 0; mapPosition.column < this.columns; mapPosition.column++ ) {
                this.ParseMapTile( rowElement.childNodes[mapPosition.column], mapPosition, imageAttributes );
            }
        }
    },
    
    ParseMapTile: function( tileElement, mapPosition, imageAttributes ) {
        this.mapTiles[mapPosition.row][mapPosition.column].walkable = (tileElement.getAttribute( "Walkable" ) === "true");

        var terrain = tileElement.getAttribute( "Terrain" );
        if( terrain != null )
            this.SetTerrainObject(mapPosition, terrain, imageAttributes[terrain].anchor);
    	
        var tile = tileElement.getAttribute( "Tile" );
        if( tile != null )
            this.SetGroundTile(mapPosition, tile);
    },
    
    ParseUnits: function( unitElement ) {
        var len = unitElement.childNodes.length;
        for( var i = 0; i < len; i++ ) {
            this.ParseGameObject( unitElement.childNodes[i] );
        }
    },
    
    ParseGameObject: function ( entityNode ) {
        var entityType = UnitTable[entityNode.nodeName];
        
        if( entityType != null ) {
            var entity = entityType.create();
            
            var mapPosition = {
                row: parseInt( entityNode.attributes.getNamedItem( "row" ).value ),
                column: parseInt( entityNode.attributes.getNamedItem( "column" ).value )
            };
            
            this.AddUnit( entity, mapPosition );
        }
    },
    
    SaveToXML: function() {
        var xmlDoc = document.implementation.createDocument( "", "", null );
        
        var root = xmlDoc.createElement( "IsoLevel" );
        xmlDoc.appendChild( root );
        
        root.appendChild( this.SerializeEditorTab(xmlDoc, "TerrainImages", EditorTabs.Terrain) );
        root.appendChild( this.SerializeEditorTab(xmlDoc, "Tiles", EditorTabs.Tiles) );
        
        root.appendChild( this.SerializeMap(xmlDoc) );
        root.appendChild( this.SerializeUnits(xmlDoc) );
        
        var serializer = new XMLSerializer();
        return serializer.serializeToString( xmlDoc );
    },
    
    SerializeEditorTab: function( xmlDoc, tagName, tab ) {
        var tabElement = xmlDoc.createElement( tagName );
    	
        for( var i = 0; i < EditorPaneObjects[tab].length; i++ ) {
            var tabObject = EditorPaneObjects[tab][i];
            if( tabObject.image != null ) {
                tabElement.appendChild( this.SerializeImage( xmlDoc, "Terrain", tabObject ) );
            }
        }
    	
        return tabElement;
    },
    
    SerializeImage: function( xmlDoc, tagName, tabObject ) {
        var imgElement = xmlDoc.createElement( tagName );
    	
        imgElement.setAttribute("name", tabObject.sprite);
        if( tabObject.anchor != null ) {
            imgElement.setAttribute("anchor-x", tabObject.anchor.x);
            imgElement.setAttribute("anchor-y", tabObject.anchor.y);
        }
        if( tabObject.rect != null ) {
            imgElement.setAttribute("rect-x", tabObject.rect.x);
            imgElement.setAttribute("rect-y", tabObject.rect.y);
            imgElement.setAttribute("rect-width", tabObject.rect.width);
            imgElement.setAttribute("rect-height", tabObject.rect.height);
        }
    	
        var imgContent = xmlDoc.createTextNode( tabObject.image.src );
    	
        imgElement.appendChild( imgContent );
    	
        return imgElement;
    },
    
    SerializeUnits: function( xmlDoc ) {
        var unitElement = xmlDoc.createElement( "Units" );
    	
        cc.log( "Serializing units: " + this.units.length );
        for( var i = 0; i < this.units.length; i++ ) {
            unitElement.appendChild( this.units[i].Serialize( xmlDoc ) );
        }
    	
        return unitElement;
    },
    
    SerializeMap: function( xmlDoc ) {
        var mapElement = xmlDoc.createElement( "Map" );
    	
        mapElement.setAttribute( "rows", this.rows );
        mapElement.setAttribute( "columns", this.columns );

        for( var row = 0; row < this.rows; row++ ) {
            var rowElement = xmlDoc.createElement( "Row" );
            for( var column = 0; column < this.columns; column++ ) {
                rowElement.appendChild( this.SerializeTile(xmlDoc, this.mapTiles[row][column]) );
            }
    		
            mapElement.appendChild( rowElement );
        }
    	
        return mapElement;
    },
    
    SerializeTile: function( xmlDoc, mapTile )
    {
        var tileElement = xmlDoc.createElement( "GroundTile" );
    	
        tileElement.setAttribute( "Walkable", mapTile.walkable );
        if( mapTile.terrainName != null )
            tileElement.setAttribute( "Terrain", mapTile.terrainName );
        if( mapTile.tileName != null )
            tileElement.setAttribute( "Tile", mapTile.tileName );
    	
        return tileElement;
    },

    WorldToIsoMapPosition: function (worldPosition) {
        var position = {};
	
        position.column = Math.floor( (worldPosition.y + (tileHeight / 2)) / tileHeight + worldPosition.x / tileWidth );
        position.row = Math.floor( (worldPosition.x + (tileWidth / 4)) / tileWidth - (worldPosition.y - (tileHeight / 4)) / tileHeight );
	
        return position;
    },

    IsoMapToWorldPosition: function (mapPosition) {
        return new cc.Point( mapPosition.row * (tileWidth / 2) + mapPosition.column * (tileWidth / 2), mapPosition.column * (tileHeight / 2) - mapPosition.row * (tileHeight / 2) );
    },
    
    UnitZOrder: function (mapPosition) {
        return 3 + (this.columns - mapPosition.column) * this.rows + mapPosition.row;
    },
	
    AddUnit: function( unit, mapPosition ) {
        unit.SetPosition(mapPosition);
        this.addChild( unit );
        this.units.push( unit );
    },
    
    RemoveUnit: function( unit ) {
        this.removeChild( unit );
        this.mapTiles[unit.row][unit.column].unit = null;
        this.units.splice( this.units.indexOf( unit ), 1 );
    },
    
    SetGroundTile: function( mapPosition, tile ) {
        this.removeChild( this.mapTiles[mapPosition.row][mapPosition.column].groundTile );
        
        var sprite = new cc.Sprite( tile );
        sprite.setPosition( this.IsoMapToWorldPosition( mapPosition ) );
        this.addChild(sprite, 1);
        
        this.mapTiles[mapPosition.row][mapPosition.column].groundTile = sprite;
        this.mapTiles[mapPosition.row][mapPosition.column].tileName = tile;
    },
    
    SetTerrainObject: function( mapPosition, terrainObject, anchor, rect ) {
        var mapTile = this.mapTiles[mapPosition.row][mapPosition.column];
        
        if( mapTile.terrainObject != null )
            this.removeChild( mapTile.terrainObject );
        
        if( terrainObject != null ) {
            sprite = new cc.Sprite( terrainObject, rect );
            sprite.setPosition( this.IsoMapToWorldPosition( mapPosition ) );
            if( anchor != null )
                sprite.setAnchorPoint( anchor );
            this.addChild(sprite, this.UnitZOrder( mapPosition ) );
        
            this.mapTiles[mapPosition.row][mapPosition.column].terrainObject = sprite;
            this.mapTiles[mapPosition.row][mapPosition.column].terrainName = terrainObject;
            this.mapTiles[mapPosition.row][mapPosition.column].walkable = false;
        }
        else {
            this.mapTiles[mapPosition.row][mapPosition.column].terrainObject = null;
            this.mapTiles[mapPosition.row][mapPosition.column].terrainName = null;
            this.mapTiles[mapPosition.row][mapPosition.column].walkable = true;
        }
    },
    
    HighlightSquare: function( row, column, color ) {
        var highlightSquare = new cc.Sprite( res.CursorSprite );
        
        highlightSquare.setColor( color );
        highlightSquare.setOpacity( 64 );
        highlightSquare.setPosition( this.IsoMapToWorldPosition( { row: row, column: column } ) );
        
        this.addChild( highlightSquare, 2 );
        this.highlightSquares.push( highlightSquare );
    },
    
    ClearHighlights: function () {
        for( var i = 0; i < this.highlightSquares.length; i++ ) {
            this.removeChild( this.highlightSquares[i], true );
        }
        
        this.highlightSquares = [];
    },
    
    GetTargetDijkstraMap: function ( faction ) {
        var tiles = [];
        var currentTiles = new BinaryHeap( function(a, b) { return a.cost - b.cost; } );
        var visitedTiles = [];
        
        for( var row = 0; row < SharedData.gameLayer.isoMap.rows; row++ ) {
            tiles[row] = [];
        }
        
        for( var i = 0; i < this.units.length; i++ ) {
            if( this.units[i].faction != faction ) {
                var currentTile = { cost: 0, previous: null, row: this.units[i].row, column: this.units[i].column };
                currentTiles.add( currentTile );
                tiles[this.units[i].row][this.units[i].column] = currentTile;
            }
        }
        
        while( currentTiles.length() > 0 ) {
            var currentTile = currentTiles.remove();
            
            var adjacentTiles = this.GetAdjacentMoves( currentTile.row, currentTile.column );
            for( var i = 0; i < adjacentTiles.length; i++ ) {
                var adjacentCost = currentTile.cost + adjacentTiles[i].cost;
                
                if( tiles[adjacentTiles[i].row][adjacentTiles[i].column] == null ) {
                    var newTile = { cost: adjacentCost, previous: currentTile, row: adjacentTiles[i].row, column: adjacentTiles[i].column };
                    currentTiles.add( newTile );
                    tiles[adjacentTiles[i].row][adjacentTiles[i].column] = newTile;
                }
                else if( tiles[adjacentTiles[i].row][adjacentTiles[i].column].cost > adjacentCost ) {
                    tiles[adjacentTiles[i].row][adjacentTiles[i].column].cost = adjacentCost;
                    tiles[adjacentTiles[i].row][adjacentTiles[i].column].previous = currentTile;
                }
            }
        }
        
        return tiles;
    },
    
    GetAdjacentMoves: function (row, column) {
        adjacentMoves = [];
        
        this.CheckAdjacentMove( row + 1,  column,     1,    adjacentMoves  );
        this.CheckAdjacentMove( row,      column + 1, 1,    adjacentMoves  );
        this.CheckAdjacentMove( row,      column - 1, 1,    adjacentMoves  );
        this.CheckAdjacentMove( row - 1,  column,     1,    adjacentMoves  );
        
        return adjacentMoves;
    },
    
    CheckAdjacentMove: function (row, column, moveCost, adjacentMoves) {
        if( !this.RangeCheck( row, column ) )
            return;
        
        if( !this.mapTiles[row][column].walkable || this.mapTiles[row][column].unit )
            return;
        
        adjacentMoves.push( {row: row, column: column, cost: moveCost} );
    },
    
    RangeCheck: function (row, column) {
        if( row < 0 || row >= this.rows )
            return false;
        if( column < 0 || column >= this.columns )
            return false;
        
        return true;
    },
    
    AddRow: function () {
        this.mapTiles.push( [] );
        var position = this.IsoMapToWorldPosition( { row: this.rows, column: 0 } );
        
        for( var column = 0; column < this.columns; column++ ) {
            this.mapTiles[this.rows][column] = this.AddTile( position );
            
            position.x += (tileWidth / 2);
            position.y += (tileHeight / 2);
        }
        
        this.rows++;
    },
    
    AddColumn: function () {
        var position = this.IsoMapToWorldPosition( { row: 0, column: this.columns } );
        
        for( var row = 0; row < this.rows; row++ ) {
            this.mapTiles[row][this.columns] = this.AddTile( position );
            
            position.x += (tileWidth / 2);
            position.y -= (tileHeight / 2);
        }
        
        this.columns++;
    },
    
    RemoveRow: function () {
        if( this.rows <= 1 )
            return;
        
        this.rows--;
        
        for( var i = 0; i < this.columns; i++ ) {
            this.RemoveTile( this.mapTiles[this.rows][i] );
        }
        
        this.mapTiles.pop();
    },
    
    RemoveColumn: function () {
        if( this.columns <= 1 )
            return;
        
        this.columns--;
            
        for( var i = 0; i < this.rows; i++ ) {
            this.RemoveTile( this.mapTiles[i].pop() );
        }
    },
    
    RemoveTile: function( mapTile ) {
        if( mapTile.unit )
            this.removeChild( mapTile.unit );
        if( mapTile.terrainObject )
            this.removeChild( mapTile.terrainObject );
        this.removeChild( mapTile.groundTile );
    },
    
    AddTile: function( position ) {
        var sprite = new cc.Sprite(res.GroundTileImage);
        sprite.setPosition( position );
        this.addChild(sprite, 1);
        
        mapTile = {
            groundTile: sprite,
            tileName: null,
            walkable: true,
            terrainObject: null,
            terrainName: null,
            unit: null
        };
        
        return mapTile;
    },
    
    ResizeMap: function( rows, columns ) {
        var deltaRows = rows - this.rows;
        var deltaColumns = columns - this.columns;
        
        if( deltaColumns > 0 ) {
            for( var i = 0; i < deltaColumns; i++ )
                this.AddColumn();
        }
        else {
            for( var i = 0; i > deltaColumns; i-- )
                this.RemoveColumn();
        }
        
        if( deltaRows > 0 ) {
            for( var i = 0; i < deltaRows; i++ )
                this.AddRow();
        }
        else {
            for( var i = 0; i > deltaRows; i-- )
                this.RemoveRow();
        }
    }
});

IsoMap.create = function( rows, columns ) {
    var isoMap = new IsoMap();
    if( isoMap && isoMap.init( rows, columns ) )
        return isoMap;
    return null; 
};

IsoMap.createWithFile = function( filename ) {
    var isoMap = new IsoMap();
    if( isoMap && isoMap.initWithFile( filename ) )
        return isoMap;
    return null; 
};

IsoMap.createWithXML = function( xml ) {
    var isoMap = new IsoMap();
    if( isoMap && isoMap.initWithXML( xml ) )
        return isoMap;
    return null; 
};