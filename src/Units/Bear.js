var Bear = MeleeUnit.extend({
    currentAP: 4,
    faction: 2,
    HP: 35,
    name: "Bear",
    
    init: function() {
        this.initWithFile( res.BearSprite, cc.rect( 0, 0, 131, 129 ) );
        this.setAnchorPoint( 0.5, 0.15 );
        
        return true;
    },
	
    Hurt: function (damage) {
        this.DamageShake();
        this.ShowFloater( damage.toString() );
        this._super(damage);
    },
    
    AttackEnemy: function (enemy) {
        this._super();
        enemy.Hurt( 16 );
    },
        
    UnitAI: function () {
        if( this.phase < PhaseCount ) {
            this.availableEnemies = this.GetAdjacentEnemies( this.row, this.column );
            if( this.availableEnemies.length > 0 ) {
                this.AttackEnemy( this.availableEnemies[0] );
                return;
            }
            
            this.availableMoves = this.GetAvailableMoves( this.currentAP );
            var dijkstraMap = SharedData.isoMap.GetTargetDijkstraMap( this.faction );
            
            var minScore = Number.MAX_VALUE;
            var chosenMove = null;
            for( var i = 0; i < this.availableMoves.length; i++ ) {
                if( dijkstraMap[this.availableMoves[i].row][this.availableMoves[i].column] ) {
                    if( dijkstraMap[this.availableMoves[i].row][this.availableMoves[i].column].cost < minScore ) {
                        minScore = dijkstraMap[this.availableMoves[i].row][this.availableMoves[i].column].cost;
                        chosenMove = this.availableMoves[i];
                    }
                }
            }
            
            if( chosenMove )
                this.Move( chosenMove );
            else
                this.phase++;
        }
    }
});

Bear.create = function() {
    var bear = new Bear();
    if( bear && bear.init() )
            return bear;
    return null; 
};