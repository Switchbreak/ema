var Ema = MeleeUnit.extend({
    currentAP: 4,
    faction: 1,
    HP: 50,
    name: "Ema",
    
    walkAnim: [],
    runningAnimation: null,
    
    init: function() {
    	cc.spriteFrameCache.addSpriteFrames(res.PlayerSprite);
    	
    	var downLeftFrames	= [];
    	var downRightFrames	= [];
    	var upLeftFrames	= [];
    	var upRightFrames	= [];
    	
    	for( var i = 1; i <= 14; i++ ) {
    		downLeftFrames.push( cc.spriteFrameCache.getSpriteFrame("DownLeft-" + i + ".png") );
    		downRightFrames.push( cc.spriteFrameCache.getSpriteFrame("DownRight-" + i + ".png") );
    		upLeftFrames.push( cc.spriteFrameCache.getSpriteFrame("UpLeft-" + i + ".png") );
    		upRightFrames.push( cc.spriteFrameCache.getSpriteFrame("UpRight-" + i + ".png") );
    	}
    	
    	this.walkAnim[0] = cc.repeatForever( cc.animate( new cc.Animation( upLeftFrames, 0.05 ) ) );
    	this.walkAnim[1] = cc.repeatForever( cc.animate( new cc.Animation( upRightFrames, 0.05 ) ) );
    	this.walkAnim[2] = cc.repeatForever( cc.animate( new cc.Animation( downLeftFrames, 0.05 ) ) );
    	this.walkAnim[3] = cc.repeatForever( cc.animate( new cc.Animation( downRightFrames, 0.05 ) ) );
    	
        this.initWithSpriteFrameName( "UpLeft-1.png" );
        this.setAnchorPoint( 0.5, 0.15 );
        
        return true;
    },

    FaceDirection: function (target, direction) {
    	if( this.runningAnimation != this.walkAnim[direction] ) {
    		this.stopAction( this.runningAnimation );
	    	this.runningAnimation = this.walkAnim[direction];
	    	this.runAction( this.runningAnimation );
    	}
    },
    
    EndMoveAnimate: function () {
    	if( this.runningAnimation ) {
    		this.stopAction( this.runningAnimation );
    		this.runningAnimation = null;
    	}
    },
    
    AttackEnemy: function (enemy) {
        this._super();
        enemy.Hurt( 16 );
    },
    
    Hurt: function (damage) {
        this.DamageShake();
        this.ShowFloater( damage.toString() );
        this._super(damage);
    }
});

Ema.create = function() {
    var ema = new Ema();
    if( ema && ema.init() )
            return ema;
    return null; 
};
