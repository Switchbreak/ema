var PhaseCount = 2;
    
var Unit = cc.Sprite.extend({
    row: 0,
    column: 0,
    faction: 0,
    HP: 0,
    phase: 0,
    name: "Unit",

    SetPosition: function (mapPosition) {
        this.row = mapPosition.row;
        this.column = mapPosition.column;

        SharedData.isoMap.mapTiles[this.row][this.column].unit = this;
        this.setPosition( SharedData.isoMap.IsoMapToWorldPosition(mapPosition) );
        this.setLocalZOrder( SharedData.isoMap.UnitZOrder( mapPosition ) );
    },
    
    MovePosition: function (mapPosition) {
        SharedData.isoMap.mapTiles[this.row][this.column].unit = null;
        this.SetPosition( mapPosition );
    },
    
    GetAvailableMoves: function ( range ) {
        var tiles = [];
        var currentTiles = new BinaryHeap( function(a, b) { return a.cost - b.cost; } );
        var visitedTiles = [];
        
        for( var row = 0; row < SharedData.isoMap.rows; row++ ) {
            tiles[row] = [];
        }
        
        var currentTile = { cost: 0, previous: null, row: this.row, column: this.column };
        currentTiles.add( currentTile );
        tiles[this.row][this.column] = currentTile;
        
        while( currentTiles.length() > 0 ) {
            currentTile = currentTiles.remove();
            visitedTiles.push( currentTile );
            
            var adjacentTiles = this.GetAdjacentMoves( currentTile.row, currentTile.column );
            for( var i = 0; i < adjacentTiles.length; i++ ) {
                var adjacentCost = currentTile.cost + adjacentTiles[i].cost;
                
                if( adjacentCost < range ) {
                    if( tiles[adjacentTiles[i].row][adjacentTiles[i].column] == null ) {
                        var newTile = { cost: adjacentCost, previous: currentTile, row: adjacentTiles[i].row, column: adjacentTiles[i].column };
                        currentTiles.add( newTile );
                        tiles[adjacentTiles[i].row][adjacentTiles[i].column] = newTile;
                    }
                    else if( tiles[adjacentTiles[i].row][adjacentTiles[i].column].cost > adjacentCost ) {
                        tiles[adjacentTiles[i].row][adjacentTiles[i].column].cost = adjacentCost;
                        tiles[adjacentTiles[i].row][adjacentTiles[i].column].previous = currentTile;
                    }
                }
            }
        }
        
        visitedTiles.shift();
        
        return visitedTiles;
    },
    
    GetAdjacentMoves: function (row, column) {
        adjacentMoves = [];
        
        this.CheckAdjacentMove( row + 1,  column,     1,    adjacentMoves  );
        this.CheckAdjacentMove( row,      column + 1, 1,    adjacentMoves  );
        this.CheckAdjacentMove( row,      column - 1, 1,    adjacentMoves  );
        this.CheckAdjacentMove( row - 1,  column,     1,    adjacentMoves  );
        
        //this.CheckAdjacentMove( row + 1,  column - 1, 1.5,  adjacentMoves );
        //this.CheckAdjacentMove( row + 1,  column + 1, 1.5,  adjacentMoves  );
        //this.CheckAdjacentMove( row - 1,  column - 1, 1.5,  adjacentMoves  );
        //this.CheckAdjacentMove( row - 1,  column + 1, 1.5,  adjacentMoves  );
        
        return adjacentMoves;
    },
    
    CheckAdjacentMove: function (row, column, moveCost, adjacentMoves) {
        if( !this.RangeCheck( row, column ) )
            return;
        
        if( !SharedData.isoMap.mapTiles[row][column].walkable || SharedData.isoMap.mapTiles[row][column].unit )
            return;
        
        adjacentMoves.push( {row: row, column: column, cost: moveCost} );
    },
    
    RangeCheck: function (row, column) {
        if( row < 0 || row >= SharedData.isoMap.rows )
            return false;
        if( column < 0 || column >= SharedData.isoMap.columns )
            return false;
        
        return true;
    },
    
    Select: function () {
        if( this.phase < PhaseCount ) {
            var highlightColor = new cc.Color( 0, 255, 0, 255 );
            this.availableMoves = this.GetAvailableMoves( this.currentAP );
            for( var i = 0; i < this.availableMoves.length; i++ ) {
                SharedData.isoMap.HighlightSquare( this.availableMoves[i].row, this.availableMoves[i].column, highlightColor );
            }
        }
    },
    
    Deselect: function() {
        this.availableMoves = null;
        SharedData.isoMap.ClearHighlights();
    },
    
    Target: function (mapPosition) {
        if( this.availableMoves != null ) {
            for( var i = 0; i < this.availableMoves.length; i++ ) {
                if( this.availableMoves[i].row == mapPosition.row && this.availableMoves[i].column == mapPosition.column ) {
                    this.Move( this.availableMoves[i] );
                    break;
                }
            }
        }
        
        this.Deselect();
    },
    
    Hurt: function (damage) {
        this.HP -= damage;
        if( this.HP <= 0 )
            this.Die();
    },
    
    Die: function () {
        SharedData.isoMap.RemoveUnit( this );
    },
    
    DamageShake: function () {
        var shakeAction = cc.Repeat.create( cc.Sequence.create( cc.MoveBy.create( 0.01, cc.p( 0, 16 ) ), cc.MoveBy.create( 0.01, cc.p( 0, -32 ) ), cc.MoveBy.create( 0.01, cc.p( 0, 16 ) ) ), 8 );
        this.runAction( shakeAction );
    },
    
    ShowFloater: function (floaterText) {
        var damageFloater = Floater.create( floaterText, new cc.Color( 255, 0, 0, 255 ) );
        damageFloater.setPosition( this.x - this.getBoundingBox().width / 2, this.y + this.getBoundingBox().height );
        this.getParent().addChild( damageFloater, 1000 );
    },
    
    Move: function (move) {
        SharedData.isoMap.mapTiles[this.row][this.column].unit = null;
        this.row = move.row;
        this.column = move.column;
        SharedData.isoMap.mapTiles[this.row][this.column].unit = this;
        this.setLocalZOrder( SharedData.isoMap.UnitZOrder( move ) );

        this.phase++;

        SharedData.gameLayer.state = GameState.Animating;

        var moveActions = [cc.callFunc( this.EndMoveAnimate, this ), cc.callFunc( SharedData.gameLayer.FactionState, SharedData.gameLayer )];
        while( move.previous ) {
        	var targetPosition = SharedData.isoMap.IsoMapToWorldPosition( {row: move.row, column: move.column} );
        	moveActions.unshift( cc.moveTo( 0.5, targetPosition ) );
        	
        	this.MoveAnimate(move, moveActions);

        	move = move.previous;
        }

        this.runAction( cc.sequence( moveActions ) );
    },
    
    MoveAnimate: function (move, moveActions) {
        if( move.row < move.previous.row )
            moveActions.unshift( cc.CallFunc.create( this.FaceDirection, this, 0 ) );
        else if( move.row > move.previous.row )
            moveActions.unshift( cc.CallFunc.create( this.FaceDirection, this, 3 ) );
        else if( move.column < move.previous.column )
            moveActions.unshift( cc.CallFunc.create( this.FaceDirection, this, 2 ) );
        else if( move.column > move.previous.column )
            moveActions.unshift( cc.CallFunc.create( this.FaceDirection, this, 1 ) );
    },

    FaceDirection: function (target, direction) {
        this.setTextureRect( cc.rect(this.width * direction, 0, this.width, this.height) );
    },
    
    EndMoveAnimate: function() { },
    
    Attack: function() {
        this.phase = PhaseCount;
    },
    
    StartTurn: function() {
        this.phase = 0;
    },
    
    EndTurn: function() {
        this.phase = PhaseCount;
    },
    
    UnitAI: function() { },
    
    Serialize: function( xmlDoc ) {
        var element = xmlDoc.createElement( this.name );
        element.setAttribute( "row", this.row );
        element.setAttribute( "column", this.column );
        
        return element;
    }
});