var MeleeUnit = Unit.extend({
    availableEnemies: null,
    
    Select: function () {
        this._super();
        
        if( this.phase < PhaseCount ) {
            var highlightColor = new cc.Color( 255, 0, 0, 255 );
            this.availableEnemies = this.GetAdjacentEnemies( this.row, this.column );
            for( var i = 0; i < this.availableEnemies.length; i++ ) {
                SharedData.isoMap.HighlightSquare( this.availableEnemies[i].row, this.availableEnemies[i].column, highlightColor );
            }
        }
    },
    
    Deselect: function () {
        this._super();
        this.availableEnemies = null;
    },
    
    Target: function (mapPosition) {
        if( this.availableEnemies != null ) {
            for( var i = 0; i < this.availableEnemies.length; i++ ) {
                if( this.availableEnemies[i].row == mapPosition.row && this.availableEnemies[i].column == mapPosition.column ) {
                    this.AttackEnemy( this.availableEnemies[i] );
                    break;
                }
            }
        }
        
        this._super(mapPosition);
    },
    
    GetAdjacentEnemies: function (row, column) {
        adjacentEnemies = [];
        
        this.CheckAdjacentEnemy( row - 1,   column,     adjacentEnemies );
        this.CheckAdjacentEnemy( row + 1,   column,     adjacentEnemies );
        this.CheckAdjacentEnemy( row,       column - 1, adjacentEnemies );
        this.CheckAdjacentEnemy( row,       column + 1, adjacentEnemies );
        
        return adjacentEnemies;
    },
    
    CheckAdjacentEnemy: function (row, column, adjacentEnemies) {
        if( !this.RangeCheck( row, column ) )
            return;
        
        var unit = SharedData.isoMap.mapTiles[row][column].unit;
        if( unit && unit.faction != this.faction )
            adjacentEnemies.push( unit );
    },
    
    AttackEnemy: function (enemy) {
        this.Attack();
    }
});