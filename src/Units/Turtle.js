var Turtle = Unit.extend({
    currentAP: 2,
    faction: 1,
    HP: 40,
    name: "Turtle",
    
    init: function() {
        this.initWithFile( res.TurtleSprite, cc.rect(0, 0, 79, 52) );
        this.setAnchorPoint( 0.55, 0.25 );
        
        return true;
    },
    
    Hurt: function (damage) {
        this.DamageShake();
        this.ShowFloater( damage.toString() );
        this._super(damage);
    }
});

Turtle.create = function() {
    var turtle = new Turtle();
    if( turtle && turtle.init() )
            return turtle;
    return null; 
};