var Mage = Unit.extend({
    currentAP: 4,
    faction: 1,
    HP: 40,
    name: "Mage",
    
    init: function() {
        this.initWithFile( res.MageSprite, cc.rect(0, 0, 58, 123) );
        this.setAnchorPoint( 0.55, 0.1 );
        
        return true;
    },
    
    Hurt: function (damage) {
        this.DamageShake();
        this.ShowFloater( damage.toString() );
        this._super(damage);
    }
});

Mage.create = function() {
    var mage = new Mage();
    if( mage && mage.init() )
            return mage;
    return null; 
};
