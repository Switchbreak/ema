var Wolf = MeleeUnit.extend({
    currentAP: 4,
    faction: 2,
    HP: 35,
    name: "Wolf",
    
    init: function() {
        this.initWithFile( res.WolfSprite, cc.rect( 0, 0, 97, 107 ) );
        this.setAnchorPoint( 0.5, 0.25 );
        
        return true;
    },
	
    Hurt: function (damage) {
        this.DamageShake();
        this.ShowFloater( damage.toString() );
        this._super(damage);
    },
    
    AttackEnemy: function (enemy) {
        this._super();
        enemy.Hurt( 16 );
    },
        
    UnitAI: function () {
        if( this.phase < PhaseCount ) {
            this.availableEnemies = this.GetAdjacentEnemies( this.row, this.column );
            if( this.availableEnemies.length > 0 ) {
                this.AttackEnemy( this.availableEnemies[0] );
                return;
            }
            
            this.availableMoves = this.GetAvailableMoves( this.currentAP );
            var dijkstraMap = SharedData.isoMap.GetTargetDijkstraMap( this.faction );
            
            var minScore = Number.MAX_VALUE;
            var chosenMove = null;
            for( var i = 0; i < this.availableMoves.length; i++ ) {
                if( dijkstraMap[this.availableMoves[i].row][this.availableMoves[i].column] ) {
                    if( dijkstraMap[this.availableMoves[i].row][this.availableMoves[i].column].cost < minScore ) {
                        minScore = dijkstraMap[this.availableMoves[i].row][this.availableMoves[i].column].cost;
                        chosenMove = this.availableMoves[i];
                    }
                }
            }
            
            
            if( chosenMove )
                this.Move( chosenMove );
            else
                this.phase++;
        }
    }
});

Wolf.create = function() {
    var wolf = new Wolf();
    if( wolf && wolf.init() )
            return wolf;
    return null; 
};