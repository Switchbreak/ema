var Seagull = MeleeUnit.extend({
    currentAP: 4,
    faction: 2,
    HP: 35,
    name: "Seagull",

    init: function() {
        this.initWithFile( res.SeagullSprite, cc.rect( 0, 0, 73, 52 ) );
        this.setAnchorPoint( 0.40, 0.1 );

        return true;
    },

    Hurt: function (damage) {
        this.DamageShake();
        this.ShowFloater( damage.toString() );
        this._super(damage);
    },
	
    AttackEnemy: function (enemy) {
        this._super();
        enemy.Hurt( 4 );
    },

    UnitAI: function () {
        if( this.phase < PhaseCount ) {
            this.availableEnemies = this.GetAdjacentEnemies( this.row, this.column );
            if( this.availableEnemies.length > 0 ) {
                this.AttackEnemy( this.availableEnemies[0] );
                return;
            }

            this.availableMoves = this.GetAvailableMoves( this.currentAP );
            var dijkstraMap = SharedData.isoMap.GetTargetDijkstraMap( this.faction );

            var minScore = Number.MAX_VALUE;
            var chosenMove = null;
            for( var i = 0; i < this.availableMoves.length; i++ ) {
                if( dijkstraMap[this.availableMoves[i].row][this.availableMoves[i].column] ) {
                    if( dijkstraMap[this.availableMoves[i].row][this.availableMoves[i].column].cost < minScore ) {
                        minScore = dijkstraMap[this.availableMoves[i].row][this.availableMoves[i].column].cost;
                        chosenMove = this.availableMoves[i];
                    }
                }
            }

            if( chosenMove )
                this.Move( chosenMove );
            else
                this.phase++;
        }
    }
});

Seagull.create = function() {
    var seagull = new Seagull();
    if( seagull && seagull.init() )
        return seagull;
    return null; 
};