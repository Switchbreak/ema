var SharedData = {};
SharedData.keyboard = [];
SharedData.selectedUnit = null;
SharedData.isoMap = null;

var ScrollSpeed = 500;
var FactionCount = 2;
var PlayerFaction = 1;

var GameState = {
    Selecting: 0,
    Animating: 1,
    AI: 2
};

var UILayer = null;

//var ParticleFog = cc.ParticleSystem.extend({
//	ctor: function() {
//		this._super(20);
//
//		var particleTexture = cc.textureCache.addImage(res.FogParticleSprite);
//		this.setTexture(particleTexture);
////		this.setSourcePosition( cc.p( 640, 0 ) );
////		this.setPosVar( cc.p( 500, 500 ) );
//		this.setDuration( cc.ParticleSystem.DURATION_INFINITY );
//		this.setEmitterMode( cc.ParticleSystem.MODE_GRAVITY );
//		this.setEmissionRate( 20 );
//		this.setLife( 20 );
//		this.setLifeVar( 10 );
//		this.setSpeed( 10 );
//		this.setSpeedVar( 5 );
//		this.setAngle( 0 );
//		this.setAngleVar( 360 );
//		this.setStartSize( 128 );
//		this.setStartSizeVar( 32 );
//		this.setEndSize( cc.ParticleSystem.START_SIZE_EQUAL_TO_END_SIZE );
//		this.setStartColor( cc.color(255, 255, 255, 50) );
//		this.setStartColorVar( cc.color(0, 0, 0, 10) );
//		this.setEndColor( cc.color(255, 255, 255, 50) );
//		this.setEndColorVar( cc.color(0, 0, 0, 10) );
//		this.positionType = cc.ParticleSystem.TYPE_GROUPED;
//		this.setBlendAdditive(true);
////		this.isAutoRemoveOnFinish( true );
//	},
//
//	initParticle: function(particle) {
//		this._super(particle);
//		
//		var row = cc.random0To1() * 576;
//		var col = cc.random0To1() * 576;
//		
//		particle.pos.x = row + col;
//		particle.pos.y = (row - col) / 2;
//	},
//});

var ParticlePath = cc.ParticleSystem.extend({
	path: null,
	interval: 0,
	
	ctor: function(path) {
		this._super();
		
		this.path = path;
		this.interval = 1 / (path.length - 1);

		var particleTexture = cc.textureCache.addImage(res.ParticleSprite);
		this.setTexture(particleTexture);
		this.setSourcePosition(this.path[0]);
		this.setDuration( cc.ParticleSystem.DURATION_INFINITY );
		this.setEmitterMode( cc.ParticleSystem.MODE_GRAVITY );
		this.setEmissionRate( 30 );
		this.setLife( 5 );
//		this.setLifeVar( 1 );
		this.setStartSize( 4 );
		this.setStartSizeVar( 0 );
		this.setEndSize( cc.ParticleSystem.START_SIZE_EQUAL_TO_END_SIZE );
//		this.setGravity( cc.p( 0, -50 ) );
		this.setPosVar( cc.p( 32, 20 ) );
		this.setStartColor( cc.color(0, 0, 0, 30) );
		this.setStartColorVar( cc.color(0, 0, 0, 0) );
		this.setEndColor( cc.color(0, 0, 0, 30) );
		this.setEndColorVar( cc.color(0, 0, 0, 0) );
		this.positionType = cc.ParticleSystem.TYPE_GROUPED;
//		this.isAutoRemoveOnFinish( true );
	},
	
	initParticle: function(particle) {
		this._super(particle);
		
		particle.variation = cc.p( particle.pos );
	},
	
	update: function (dt) {
		this._particleIdx = 0;
		var locParticles = this._particles;
		while (this._particleIdx < this.particleCount) {
			var selParticle = locParticles[this._particleIdx];
			
			var progress = (this.life - selParticle.timeToLive) / this.life;
			cc.pIn( selParticle.pos, cc.pAdd( this.positionOnPath( progress ), selParticle.variation ) );
			
			++this._particleIdx;
		}
		
		this._super(dt);
	},
	
	positionOnPath: function( progress ) {
		if( progress <= 0 )
			return this.path[0];
		if( progress >= 1 )
			return this.path[this.path.length - 1];
		
		var intervalProgress = progress / this.interval;
		var index = Math.floor( intervalProgress );
		intervalProgress -= index;
		
		var source = this.path[index];
		var target = this.path[index + 1];
		
		var ret = cc.pSub( cc.pLerp(source, target, intervalProgress), this.sourcePos );
		return ret;
	}
});

var EmaLayer = cc.Layer.extend({
    background: null,
	isoMap: null,
    cursor: null,
    playerPhasePane: null,
    currentFaction: 1,
    state: GameState.Selecting,
    touchHeld: false,

    init: function () {
        this._super();
        SharedData.gameLayer = this;
        
        this.background = new cc.Sprite( res.BackgroundImage );
        this.background.setAnchorPoint( 0, 1 );
        this.background.y = this.height;
        this.addChild( this.background );
        
        this.isoMap = IsoMap.createWithFile( res.Level2XML );
        this.addChild( this.isoMap );
        
        this.InitUI();
        this.InitInput();
        
        this.scheduleUpdate();
        this.StartTurn();
    },
    
    InitInput: function() {
    	var touchAction = cc.sequence( [cc.delayTime(0.2), cc.callFunc( function() { this.touchHeld = true; }, SharedData.gameLayer )] );
    	
        cc.log( "Initializing input..." );
        
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed: function(keyCode, event) { SharedData.keyboard[keyCode] = true; },
            onKeyReleased: function(keyCode, event) {
            	SharedData.keyboard[keyCode] = false;

            	if( keyCode == cc.KEY.space )
            		SharedData.gameLayer.EndTurn();
            	if( keyCode == cc.KEY.q )
            		SharedData.gameLayer.ExitGame();
            }
        }, this);

        cc.eventManager.addListener({
        	event: cc.EventListener.TOUCH_ONE_BY_ONE,
        	onTouchBegan: function( touch, event ) {
        		if( SharedData.gameLayer.playerPhasePane.visible && touch.getLocationY() < SharedData.gameLayer.playerPhasePane.height )
        			return false;

        		SharedData.gameLayer.touchHeld = false;
        		SharedData.gameLayer.runAction( touchAction );

        		return true;
        	},
        	onTouchMoved: function( touch, event ) {
        		var delta = cc.pSub( touch.getLocation(), touch.getPreviousLocation() );
                
        		SharedData.isoMap.x += delta.x;
        		SharedData.isoMap.y += delta.y;
        	},
        	onTouchEnded: function( touch, event ) {
        		if( !SharedData.gameLayer.touchHeld && SharedData.gameLayer.state == GameState.Selecting ) {
                    var mousePosition = touch.getLocation();
                    var mapPosition = SharedData.isoMap.WorldToIsoMapPosition( SharedData.isoMap.convertToNodeSpace( mousePosition ) );
                    if( SharedData.isoMap.RangeCheck( mapPosition.row, mapPosition.column ) ) {
                        SharedData.gameLayer.moveCursor( mapPosition );
                        var unit = SharedData.isoMap.mapTiles[mapPosition.row][mapPosition.column].unit;
                    
                        if( unit && unit.faction == SharedData.gameLayer.currentFaction )
                            SharedData.gameLayer.SelectUnit( unit );
                        else
                            SharedData.gameLayer.TargetSquare( mapPosition );
                    }
                    
                    return true;
                }
            }
        }, this);
    },
    
    InitUI: function () {
        this.playerPhasePane = new PlayerPhasePane( this.EndTurn, this.selectNextUnit, this.selectPrevUnit );
        this.playerPhasePane.visible = false;
        UILayer.addChild( this.playerPhasePane );
        
        this.cursor = new cc.Sprite( res.CursorSprite );
        this.cursor.setOpacity( 128 );
        this.isoMap.addChild( this.cursor, 2 );
        
        var riverPath = [
                         this.isoMap.IsoMapToWorldPosition( { row: 0, column: 7 } ),
                         this.isoMap.IsoMapToWorldPosition( { row: 0, column: 6 } ),
                         this.isoMap.IsoMapToWorldPosition( { row: 2, column: 6 } ),
                         this.isoMap.IsoMapToWorldPosition( { row: 5, column: 6 } ),
                         this.isoMap.IsoMapToWorldPosition( { row: 5, column: 5 } ),
                         this.isoMap.IsoMapToWorldPosition( { row: 8, column: 5 } ),
                         this.isoMap.IsoMapToWorldPosition( { row: 8, column: 4 } ),
                         this.isoMap.IsoMapToWorldPosition( { row: 9, column: 4 } ),
                         ];
        this.particles = new ParticlePath(riverPath);
        this.isoMap.addChild(this.particles, 2);
        
        this.fogParticles = [];
        for( var i = 0; i < 50; i++ ) {
        	var fogParticle = new ParticleFog();
        	this.isoMap.addChild(fogParticle);
        	this.fogParticles.push(fogParticle);
        }
    },
    
    StartTurn: function() {
        this.ShowFactionLabel();
    },
    
    EndTurn: function () {
        this.DeselectUnit();
        
        var oldFaction = this.currentFaction;
        this.currentFaction = (this.currentFaction % FactionCount) + 1;
        
        for( var i = 0; i < this.isoMap.units.length; i++ ) {
            if( this.isoMap.units[i].faction == this.currentFaction )
                this.isoMap.units[i].StartTurn();
            else if( this.isoMap.units[i].faction == oldFaction )
                this.isoMap.units[i].EndTurn();
        }
        
        this.StartTurn();
    },
    
    ExitGame: function () {
        cc.director.runScene( new MenuScene() );
    },
    
    ShowFactionLabel: function () {
        this.state = GameState.Animating;
        
        var frameSize = cc.director.getWinSize();
        var phaseLabel = Floater.create( "Faction " + this.currentFaction + " turn", new cc.Color( 255, 255, 255, 255 ) );
        phaseLabel.setPosition( frameSize.width / 2, frameSize.height / 2 );
        UILayer.addChild( phaseLabel, 1000 );
        
        this.runAction( new cc.Sequence( new cc.DelayTime( 0.5 ), new cc.CallFunc( this.FactionState, this ) ) );
    },
    
    FactionAI: function () {
        for( var i = 0; i < this.isoMap.units.length; i++ ) {
            if( this.isoMap.units[i].faction == this.currentFaction && this.isoMap.units[i].phase < PhaseCount ) {
                this.centerCameraOnUnit( this.isoMap.units[i] );
                this.isoMap.units[i].UnitAI();
                return;
            }
        }
        
        this.EndTurn();
    },
    
    FactionState: function () {
        if( this.currentFaction == PlayerFaction ) {
            this.state = GameState.Selecting;
            
            if( SharedData.selectedUnit )
                this.SelectUnit( SharedData.selectedUnit );
            else
                this.selectFirstUnit();
        }
        else
            this.state = GameState.AI;
    },
    
    update: function( dt ) {
        this.scrollInput( dt );
        if( this.state == GameState.AI )
            this.FactionAI();
    },
    
    scrollInput: function( dt ) {
        if( SharedData.keyboard[cc.KEY.up] )
            this.isoMap.y -= ScrollSpeed * dt;
        if( SharedData.keyboard[cc.KEY.down] )
            this.isoMap.y += ScrollSpeed * dt;
        if( SharedData.keyboard[cc.KEY.left] )
            this.isoMap.x += ScrollSpeed * dt;
        if( SharedData.keyboard[cc.KEY.right] )
            this.isoMap.x -= ScrollSpeed * dt;
    },
    
    selectFirstUnit: function() {
        for( var i = 0; i < this.isoMap.units.length; i++ ) {
            if( this.isoMap.units[i].faction == this.currentFaction && this.isoMap.units[i].phase < PhaseCount ) {
                this.SelectUnit( this.isoMap.units[i] );
                return;
            }
        }
    },
    
    selectNextUnit: function() {
        this.cycleUnit( 1 );
    },
    
    selectPrevUnit: function() {
        this.cycleUnit( -1 );
    },
    
    cycleUnit: function( direction ) {
        if( !SharedData.selectedUnit )
            this.selectFirstUnit();
            
        var unitIndex = this.isoMap.units.indexOf( SharedData.selectedUnit );
        var unitCount = this.isoMap.units.length;
        
        for( var i = 0; i < unitCount; i++ ) {
            unitIndex = (unitIndex + unitCount + direction) % this.isoMap.units.length;
            if( this.isoMap.units[unitIndex].faction == SharedData.selectedUnit.faction && this.isoMap.units[unitIndex].phase < PhaseCount ) {
                this.SelectUnit( this.isoMap.units[unitIndex] );
                return;
            }
        }
    },
    
    centerCameraOnUnit: function( unit ) {
        var frameSize = cc.director.getWinSize();
        this.isoMap.x = (frameSize.width / 2) - unit.x;
        this.isoMap.y = (frameSize.height / 2) - unit.y;
    },
    
    moveCursor: function( mapPosition ) {
        this.cursor.setPosition( this.isoMap.IsoMapToWorldPosition( mapPosition ) );
    },
       
    SelectUnit: function( unit ) {
        this.DeselectUnit();

        SharedData.selectedUnit = unit;
        SharedData.selectedUnit.Select();
        
        this.cursor.setPosition( this.isoMap.IsoMapToWorldPosition( { row: unit.row, column: unit.column } ) );
        this.playerPhasePane.updatePanel( unit );
        this.ShowPlayerPhasePane();
        this.centerCameraOnUnit( unit );
    },
    
    DeselectUnit: function() {
        if( SharedData.selectedUnit ) {
            SharedData.selectedUnit.Deselect();
            SharedData.selectedUnit = null;
        }
            
        this.HidePlayerPhasePane();
    },
    
    TargetSquare: function( mapPosition ) {
        if( SharedData.selectedUnit )
            SharedData.selectedUnit.Target( mapPosition );
    },
    
    ShowPlayerPhasePane: function() {
        this.playerPhasePane.visible = true;
        
        this.background.scaleY = (this.height - this.playerPhasePane.height) / this.height;
    },
    
    HidePlayerPhasePane: function() {
        this.playerPhasePane.visible = false;
        this.background.scaleY = 1;
    }
});

var EmaScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        
        var layer = new EmaLayer();
        UILayer = new cc.Layer();
        
        this.addChild(layer);
        this.addChild(UILayer);
        
        UILayer.init();
        layer.init();        
    }
});
