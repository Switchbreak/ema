function BinaryHeap( compare ) {
    var heap = [];
    var compareFunction = compare;
    
    this.add = function (item) {
        var index = heap.length;
        heap[index] = item;

        while( index > 0 ) {
            var parent = Math.floor((index - 1) / 2);
            
            if( compareFunction( heap[index], heap[parent] ) < 0 ) {
                heap[index] = heap[parent];
                heap[parent] = item;
            }
            else {
                break;
            }
            
            index = parent;
        }
    };
    
    this.remove = function () {
        if( heap.length > 1 ) {
            var topItem = heap[0];
            heap[0] = heap.pop();
        
            var index = 0;
            while( index < heap.length ) {
                var leftChild = index * 2 + 1;
                var rightChild = leftChild + 1;
                
                var min = index;
                if( leftChild < heap.length && compareFunction( heap[leftChild], heap[min] ) < 0 )
                    min = leftChild;
                if( rightChild < heap.length && compareFunction( heap[rightChild], heap[min] ) < 0 )
                    min = rightChild;
                
                if( min == index ) {
                    break;
                }
                
                var temp = heap[index];
                heap[index] = heap[min];
                heap[min] = temp;
                index = min;
            }
            
            return topItem;
        }
        else {
            return heap.pop();
        }
    };
    
    this.length = function () {
        return heap.length;
    };
}
