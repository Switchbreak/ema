cc.game.onStart = function(){
	//var policy = new cc.ResolutionPolicy( cc.ContainerStrategy.ORIGINAL_CONTAINER, cc.ContentStrategy.SHOW_ALL );
    //cc.view.setDesignResolutionSize(640, 480, policy);
	cc.view.resizeWithBrowserSize(false);
	cc.view.enableAutoFullScreen(false);
	//load resources
    cc.LoaderScene.preload(g_resources, function () {
        cc.director.runScene(new MenuScene());
    }, this);
};
cc.game.run();